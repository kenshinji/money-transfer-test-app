package error;

public class InsufficientBalanceException extends Exception {
    public InsufficientBalanceException() {
        super("Can't perform this operations due to insufficient account balance");
    }

    public InsufficientBalanceException(String message) {
        super(message);
    }
}
