package error;

public class DuplicateAccountException extends Exception {

    public DuplicateAccountException(String accountId) {
        super("Account with ID:" + accountId + " already exists. Duplicates are not allowed.");
    }
}
