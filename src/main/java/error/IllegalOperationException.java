package error;

public class IllegalOperationException extends Exception {

    public IllegalOperationException() {
        super("Operation can't be performed.");
    }

    public IllegalOperationException(String message) {
        super(message);
    }
}
