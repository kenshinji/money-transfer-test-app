package models;

import java.math.BigDecimal;

final public class Transaction {

    private final String source;
    private final String target;
    private final BigDecimal amount;

    public Transaction(String source, String target, String amount) {
        this.source = source;
        this.target = target;
        this.amount = new BigDecimal(amount);
    }

    public Transaction() {
        source = "";
        target = "";
        amount = BigDecimal.ZERO;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "MoneyTransfer | amount=" + amount + " " +
                "from sourceAccount='" + source + '\'' +
                "to targetAccount='" + target;
    }
}
