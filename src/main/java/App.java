

import com.google.gson.Gson;

import dao.AccountRepository;
import error.DuplicateAccountException;
import error.IllegalOperationException;
import error.InsufficientBalanceException;
import models.Account;
import models.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.TransactionService;
import spark.Spark;

import java.util.ArrayList;
import java.util.List;


import static spark.Spark.get;
import static spark.Spark.post;

public class App {

    private final static Logger logger = LoggerFactory.getLogger(App.class.getCanonicalName());
    private static final AccountRepository repository = AccountRepository.getInstance();
    private static final TransactionService transactionService = TransactionService.getInstance();

    public static void main(String[] args) {

        // register a catch-all Spark exception handler to log uncaught exceptions:
        Spark.exception(Exception.class, (exception, request, response) -> {
            exception.printStackTrace();
        });



        get("/accounts", (req, res) -> {
            return new Gson().toJson(repository.getAll());
        });

        get("/accounts/:id", (req, res) -> {
            String id = req.params("id");
            Account account = repository.getById(id);
            if (account == null) {
                res.status(204);
                return "";
            }
            return new Gson().toJson(account);
        });


        post("/accounts", "application/json", (req, res) -> {
            Account newAccount = new Gson().fromJson(req.body(), Account.class);
            try {
                newAccount = repository.addAccount(newAccount);
            } catch (DuplicateAccountException e) {
                logger.error(e.getMessage());
                res.status(400);
                return "Account already exists";
            }
            res.status(201);
            return new Gson().toJson(newAccount);
        });

        post("transactions", "application/json", (req, res) -> {
            Transaction transaction = new Gson().fromJson(req.body(), Transaction.class);
            List<Account> result = new ArrayList<>();
            try {
                result = transactionService.transfer(transaction);
            } catch (IllegalOperationException e) {
                logger.error(e.getMessage());
                res.status(400);
                return e.getMessage();

            } catch (InsufficientBalanceException e) {
                logger.error(e.getMessage());
                res.status(409);
                return e.getMessage();
            }

            return new Gson().toJson(result);
        });

    }

}
