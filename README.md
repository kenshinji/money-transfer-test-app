# Money Transfer App
Money Transfer APIs supporting concurrent calls

Application starts webserver on http://localhost:4567 by default


## How to run this application

You can either run this app as a standalone jar file
```sh
java -jar money-transfer-app-1.0-SNAPSHOT-jar-with-dependencies.jar
```
or as a maven goal

```sh
mvn exec:java
```
## Account API - `/accounts`

**GET** - retrieves all accounts from database

Response:
**Status: 200 OK**
```javascript
[
    {
        "id": "1",
        "balance": 80.1
    },
    {
        "id": "2",
        "balance": 15
    },
    {
        "id": "3",
        "balance": 130.1
    },
    {
        "id": "4",
        "balance": 100.1
    }
]
```
---
**POST** - persists new account
**Request Body** - Account object

Sample request:
```javascript
{
	"id":"2",
	"balance":"5.0"
}
```

Sample response:
**Status: 200 OK**
```javascript
{
	"id":"2",
	"balance":"5.0"
}
```
Duplicated account response:
**Status: 400 Bad Request**
```javascript
Account with ID:2 already exists.
Duplicates are not allowed.
```
---
**/{id}** - account id
**GET** - retrieves specific account from database

Response:
**Status: 200 OK**
```javascript
{
    "id": "3",
    "balance": 55.3
}
```
Account doesn't exist:
**Status: 204 No Content**

## Transaction API - `/transactions`

**POST** - submit new transaction

**Request Body** - MoneyTransfer object

Sample request:
```javascript
{
	"source":"1",
	"target":"2",
	"amount":"5.0"
}
```

Sample response:
**Status: 200 OK**
```javascript
[
    {
        "id": "1",
        "balance": "45"
    },
    {
        "id": "2",
        "balance": "10"
    }
]
```

Insufficient balance on source account:
**Status: 409 Conflict**
```javascript
Can't perform this operations due to insufficient account balance
```

One of the party accounts doesn't exist:
**Status: 400 Bad Request**
```javascript
Account(s) doesnt exist. | Source: null, Target: Account{id=2, balance=10}
```
